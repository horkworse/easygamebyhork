﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyGameByHork
{
    class Weapon
    {
        public int Damage { get; set; }
        public int Ammo{ get; set; }
        public int Reload { get; set; }

        public Weapon(int damage, int ammo, int reload)
        {
            this.Damage = damage;
            this.Ammo = ammo;
            this.Reload = reload;
        }
    }
}

