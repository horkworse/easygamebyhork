﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace EasyGameByHork
{
    public partial class Main : Form
    {
        private readonly Hero Hero;
        private readonly List<Enemy> Enemies;
        public Bitmap Bmp;
        public Graphics Graph;
        public bool IsPressed = false;
        public int Prev;
        public bool IsStopped = false;
        public int Score = 0;
        Size Map;
        static readonly object locker = new object();
        readonly Dictionary<string, Image> Sprites;
        readonly List<Bonus> Bonuses;

        public Main()
        {
            InitializeComponent();
            Sprites = new Dictionary<string, Image>
            {
                { "moveLeft", Image.FromFile("C:/Users/Hork/source/repos/EasyGameByHork/EasyGameByHork/images/moveLeft.png") },
                { "moveRight", Image.FromFile("C:/Users/Hork/source/repos/EasyGameByHork/EasyGameByHork/images/moveRight.png") },
                { "standLeft", Image.FromFile("C:/Users/Hork/source/repos/EasyGameByHork/EasyGameByHork/images/standLeft.png") },
                { "standRight", Image.FromFile("C:/Users/Hork/source/repos/EasyGameByHork/EasyGameByHork/images/standRight.png") },
                { "fireLeft", Image.FromFile("C:/Users/Hork/source/repos/EasyGameByHork/EasyGameByHork/images/fireLeft.png") },
                { "fireRight", Image.FromFile("C:/Users/Hork/source/repos/EasyGameByHork/EasyGameByHork/images/fireRight.png") },
                { "enemyLeft", Image.FromFile("C:/Users/Hork/source/repos/EasyGameByHork/EasyGameByHork/images/enemyLeft.png") },
                { "enemyRight", Image.FromFile("C:/Users/Hork/source/repos/EasyGameByHork/EasyGameByHork/images/enemyRight.png") },
                { "enemyRightTrigger", Image.FromFile("C:/Users/Hork/source/repos/EasyGameByHork/EasyGameByHork/images/enemyRightTrigger.png") },
                { "enemyLeftTrigger", Image.FromFile("C:/Users/Hork/source/repos/EasyGameByHork/EasyGameByHork/images/enemyLeftTrigger.png") },
                { "triggerRight", Image.FromFile("C:/Users/Hork/source/repos/EasyGameByHork/EasyGameByHork/images/triggerRight.png") },
                { "triggerLeft", Image.FromFile("C:/Users/Hork/source/repos/EasyGameByHork/EasyGameByHork/images/triggerLeft.png") },
                { "bonus", Image.FromFile("C:/Users/Hork/source/repos/EasyGameByHork/EasyGameByHork/images/bonus.png") },
                { "HP", Image.FromFile("C:/Users/Hork/source/repos/EasyGameByHork/EasyGameByHork/images/HP.png") },
                { "bg", Image.FromFile("C:/Users/Hork/source/repos/EasyGameByHork/EasyGameByHork/images/background.png") }
            };
            Map = new Size(Canvas.Width, Canvas.Height);
            Bmp = new Bitmap(Map.Width, Map.Height);
            Bmp.MakeTransparent(Color.White);
            Graph = Graphics.FromImage(Bmp);
            Enemies = new List<Enemy>();
            Bonuses = new List<Bonus>();
            Hero = new Hero(new Point(Map.Width / 2, Map.Height - 100), HP.Human, Sprites["standRight"], Direction.Right);
            Draw();
        }

        private void Main_KeyDown(object sender, KeyEventArgs e)
        {
            if (!IsStopped)
            {
                if (!IsPressed)
                {
                    switch (e.KeyCode)
                    {
                        case Keys.Right:
                            Hero.Move(Direction.Right, Step.X2, Map);
                            Hero.Image = Sprites["moveRight"];
                            Hero.CurDir = Direction.Right;
                            break;
                        case Keys.Left:
                            Hero.Move(Direction.Left, Step.X2, Map);
                            Hero.Image = Sprites["moveLeft"];
                            Hero.CurDir = Direction.Left;
                            break;
                        case Keys.Up:
                            Hero.Move(Direction.Top, Step.X2, Map);
                            Hero.Image = Hero.CurDir == Direction.Right ? Sprites["moveRight"] : Sprites["moveLeft"];
                            Hero.CurDir = Hero.CurDir == Direction.Right ? Direction.Right : Direction.Left;
                            break;
                        case Keys.Down:
                            Hero.Move(Direction.Bottom, Step.X2, Map);
                            Hero.Image = Hero.CurDir == Direction.Right ? Sprites["moveRight"]: Sprites["moveLeft"];
                            Hero.CurDir = Hero.CurDir == Direction.Right ? Direction.Right : Direction.Left;
                            break;
                        case Keys.Space:
                            var image = Hero.CurDir == Direction.Right ? Sprites["fireRight"] : Sprites["fireLeft"];
                            Hero.Image = image;

                            foreach (var enemy in Enemies)
                            {
                                if (Hero.CurDir != enemy.CurDir &&
                                    Math.Abs(Hero.X - enemy.X) <= 200 &&
                                    Math.Abs(Hero.Y - enemy.Y) <= 20)
                                {
                                    if (enemy.HP < 0)
                                    {
                                        Enemies.Remove(enemy);
                                        ScoreBox.Text = (++Score).ToString();
                                        break;
                                    }
                                    else
                                    {
                                        enemy.HP -= Hero.Weapon.Damage;
                                        enemy.IsAtacked = true;
                                        enemy.Image = enemy.CurDir == Direction.Right ? Sprites["enemyRightTrigger"] : Sprites["enemyLeftTrigger"];
                                        break;
                                    }
                                }
                            }
                            Draw();
                            break;
                        default: break;
                    }
                    SearchForBonus();
                    Draw();
                    IsPressed = true;
                }
            }
        }

        public void SearchForBonus()
        {
            //if (Bonuses.Count == 0)
            //    return;

            //lock (locker)
            //{
            //    foreach (var b in Bonuses)
            //    {
            //        if (Hero.X - b.X <= 20 && Hero.Y - b.Y <= 10)
            //        {
            //            if (b.Property == Property.HP)
            //                Hero.HP += 20;
            //            else
            //                Hero.Weapon.Damage += 10;
            //            Bonuses.Remove(b);
            //        }
            //    }
            //}
        }

        private void Main_KeyUp(object sender, KeyEventArgs e)
        {
            IsPressed = false;
        }

        public void Draw()
        {
            lock (locker)
            {
                Graph.Clear(Color.White);
                Graph.DrawImage(Sprites["bg"], new Point(0, 0));
                Graph.DrawImage(Hero.Image, new Point(Hero.X, Hero.Y));
                DrawEnemies();
                DrawBonuses();
                Canvas.Image = Bmp;
                timer.Enabled = true;
                timer.Interval = 250;
            }
        }

        public void DrawEnemies()
        {
            foreach (var e in Enemies)
                Graph.DrawImage(e.Image, new Point(e.X, e.Y));
        }

        public void DrawBonuses()
        {
            foreach (var e in Bonuses)
                Graph.DrawImage(e.Image, new Point(e.X, e.Y));
        }

        private void GenerateEnemies()
        {
            var rnd = new Random();
            var xValue = rnd.Next(1, 3);
            var yValue = rnd.Next(0, 4);
            int xPos;
            var yPos = 310 + (20 * yValue);
            Direction dir;
            Image img;
            if (xValue == 2)
            {
                img = Sprites["enemyLeft"];
                xPos = Map.Width - 100;
                dir = Direction.Left;
            }
            else
            {
                xPos = 50;
                img = Sprites["enemyRight"];
                dir = Direction.Right;
            }
            Enemies.Add(new Enemy( new Point(xPos, yPos), HP.Low, 10, img, dir));
            Draw();
        }

        private void GenerateBonus()
        {
            var rnd = new Random();
            var xValue = rnd.Next(200, 500);
            var yValue = rnd.Next(310, 370);
            var prop = rnd.Next(1, 3);
            var img = (Property)prop != Property.HP ? Sprites["HP"] : Sprites["bonus"];
            Bonuses.Add(new Bonus(new Point(xValue, yValue), (Property)prop, img));
            Draw();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            lock (locker)
            {
                Graph.DrawImage(Sprites["bg"], new Point(0, 0));
                var image = Hero.CurDir == Direction.Right ? Sprites["standRight"] : Sprites["standLeft"];
                Graph.DrawImage(image, new Point(Hero.X, Hero.Y));
                DrawEnemies();
                DrawBonuses();
                Canvas.Image = Bmp;
            }
        }

        private void Main_Shown(object sender, EventArgs e)
        {
            var step = 0;
            var bonusTime = 0;
            var timer = new System.Timers.Timer
            {
                Interval = 200
            };
            timer.Start();
            timer.Elapsed += (sender1, args) =>
            {
                timer.Enabled = false;
                hpBox.Invoke(new Action(() => hpBox.Text = ((int)Hero.HP).ToString()));
                if (!IsStopped)
                {
                    if (step == 5)
                    {
                        GenerateEnemies();
                        step = 0;
                        if (bonusTime == 0)
                        {
                            bonusTime = 0;
                            GenerateBonus();
                        }

                        else
                            bonusTime++;
                    }
                    else
                        step++;
                    foreach (var enemy in Enemies)
                    {
                        if (enemy.X > Hero.X)
                        {
                            enemy.Image = Sprites["enemyLeft"];
                            enemy.CurDir = Direction.Left;
                            enemy.Move(Direction.Left, Step.X1, Map);
                        }
                        else if (enemy.X < Hero.X)
                        {
                            enemy.Image = Sprites["enemyRight"];
                            enemy.CurDir = Direction.Right;
                            enemy.Move(Direction.Right, Step.X1, Map);
                        }

                        if (enemy.Y > Hero.Y)
                        {
                            enemy.Image = enemy.CurDir == Direction.Right ? Sprites["enemyRight"] : Sprites["enemyLeft"]; ;
                            enemy.CurDir = enemy.CurDir == Direction.Right ? Direction.Right : Direction.Left;
                            enemy.Move(Direction.Top, Step.X1, Map);
                        }
                        else if (enemy.Y < Hero.Y)
                        {
                            enemy.Image = enemy.CurDir == Direction.Right ? Sprites["enemyRight"] : Sprites["enemyLeft"]; ;
                            enemy.CurDir = enemy.CurDir == Direction.Right ? Direction.Right : Direction.Left;
                            enemy.Move(Direction.Bottom, Step.X1, Map);
                        }

                        if (Math.Abs(enemy.X - Hero.X) < 40 &&
                            Math.Abs(enemy.Y - Hero.Y) < 20)
                        {
                            Hero.Image = Hero.CurDir == Direction.Right ? Sprites["triggerRight"] : Sprites["triggerLeft"];
                            Hero.IsAtacked = true;
                            Hero.HP -= enemy.Damage;
                            Draw();
                        }
                    }
                    if (Hero.HP <= 0)
                    {
                        IsStopped = true;
                        timer.Enabled = false;
                        EndGame();
                    }
                }
                timer.Enabled = true;
            };
        }

        private void EndGame()
        {
            MessageBox.Show("gameover");
        }
    }
}
