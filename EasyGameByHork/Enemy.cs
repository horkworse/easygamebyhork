﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyGameByHork
{
    class Enemy : Unit
    {
        public int Damage { get; set; }

        public Enemy(Point point, HP hp, int damage, Image img, Direction dir)
        {
            this.X = point.X;
            this.Y = point.Y;
            this.HP = hp;
            this.Damage = damage;
            this.Image = img;
            this.CurDir = dir;
        }

        public void Atack (Hero hero, int damage)
        {
            hero.HP -= damage;
        }

    }
}
