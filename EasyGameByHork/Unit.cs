﻿using EasyGameByHork.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace EasyGameByHork
{
    class Unit
    {
        public int X { get; set; }
        public int Y { get; set; }
        public HP HP { get; set; }

        public bool IsAtacked = false;

        public Image Image;

        public Direction CurDir;

        public void Move(Direction dir, Step step, Size map)
        {
            switch(dir)
            {
                case Direction.Left:
                    if (this.X > 20)
                        this.X -= (int)step;
                    break;
                case Direction.Right:
                    if (this.X < map.Width - 70)
                        this.X += (int)step;
                    break;
                case Direction.Top:
                    if (this.Y > 300)
                        this.Y -= (int)step;
                    break;
                case Direction.Bottom:
                    if (this.Y < map.Height - 80)
                        this.Y += (int)step;
                    break;
                default:
                    throw new NotImplementedException();
            }
        }
    }
}
