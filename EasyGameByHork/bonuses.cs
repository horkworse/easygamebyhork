﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyGameByHork
{
    class Bonus
    {
        public int X { get; set; }
        public int Y { get; set; }
        public Property Property { get; set; }
        public Image Image;

        public Bonus(Point point, Property prop, Image img)
        {
            this.X = point.X;
            this.Y = point.Y;
            this.Property = prop;
            this.Image = img;
        }
    }
}
