﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyGameByHork
{
    enum Direction
    {
        Top,
        Right,
        Bottom,
        Left
    }

    enum State
    {
        Left,
        LeftMove,
        Right,
        RightMove,
        Bg
    }

    enum Step
    {
        X1 = 10,
        X2 = 20,
        X3 = 30
    }

    enum Level
    {
        Easy,
        Medium,
        Hard
    }

    enum HP
    {
        Low = 10,
        Medium = 20,
        Hight = 30,
        Human = 100
    }

    enum Property
    {
        HP = 1,
        Atack = 2
    }
}
