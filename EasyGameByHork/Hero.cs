﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyGameByHork
{
    class Hero : Unit
    {
        public Weapon Weapon { get; set; }

        public Hero (Point point, HP hp, Image img, Direction dir)
        {
            this.X = point.X;
            this.Y = point.Y;
            this.HP = hp;
            this.Weapon = new Weapon(10, 50, 5);
            this.Image = img;
            this.CurDir = dir;
        }
    }
}
